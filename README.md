Grid layout populated dynamically with squares of 40x40 dp with margins of 2dp.

The app will dynamically create columns and rows to fit any size of screen.
The number of columns and rows will vary according to the screen size.
the same order of colors will be retained after rotating the device.
clicking on any square will change into a different random color. 
the new color will be retained during device rotation. 
