package com.heshamfas.color_button.buttonfun;

import android.graphics.Color;

import java.util.Random;

/**
 * Created by hf731n on 3/24/2017.
 */

public class ColorUtils {

    private static Random random =new Random(System.currentTimeMillis());

    private static int getRandomRGB(){
        return random.nextInt(256);
    }

    public static int getRandomColor(){
        return Color.rgb(getRandomRGB(),getRandomRGB(),getRandomRGB());
    }


}
