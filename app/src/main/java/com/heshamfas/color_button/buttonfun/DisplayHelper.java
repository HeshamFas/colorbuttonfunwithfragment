package com.heshamfas.color_button.buttonfun;

import android.app.Activity;
import android.util.DisplayMetrics;

/**
 * Created by hf731n on 3/24/2017.
 */

public class DisplayHelper {

    Activity mContext;
    int viewLength = 0;
    int windowWidth;
    int windowHeight;
    int columns =0;
    int rows = 0;

    public DisplayHelper(Activity context){
        mContext = context;
        viewLength=(int)(context.getResources().getDimension(R.dimen.color_button_width) +
                2 * context.getResources().getDimension(R.dimen.color_button_margin));
        setScreenWidthAndHeight();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        mContext.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        setNumberOfColums();
        setNumberOfRows();
    }
    
    private void setScreenWidthAndHeight(){
        DisplayMetrics displayMetrics = new DisplayMetrics();
        mContext.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        windowWidth = displayMetrics.widthPixels;
        windowHeight = displayMetrics.heightPixels;
    }
    private void setNumberOfColums(){
         columns =windowWidth/ viewLength;
    }

    private void setNumberOfRows(){
        rows= windowHeight/ viewLength;
    }
    public int getViewLength() {
        return viewLength;
    }


    public int getWindowWidth() {
        return windowWidth;
    }
    public int getWindowheight() {
        return windowHeight;
    }
    private int getRightEmptySpace()
    {
        return windowWidth - (viewLength * getColumns());
    }
    private int getBottomEmptySpace(){
        return windowHeight -(viewLength * getRows());
    }
    public int getVerticalPadding(){
        return getBottomEmptySpace()/2;
    }
    public int getSidePadding(){
        return getRightEmptySpace()/2;
    }
    public int getColumns() {
        return columns;
    }

    public int getRows() {
        return rows;
    }

}
