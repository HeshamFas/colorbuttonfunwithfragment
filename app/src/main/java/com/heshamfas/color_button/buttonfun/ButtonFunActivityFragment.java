package com.heshamfas.color_button.buttonfun;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;
import android.widget.ImageButton;
import static com.heshamfas.color_button.buttonfun.ColorUtils.getRandomColor;
/**
 * A placeholder fragment containing a simple view.
 */
public class ButtonFunActivityFragment extends Fragment implements View.OnClickListener{
    private static final String COLOR_ARRAY = "colorArray";
    GridLayout actionFunGrid;
    LayoutInflater inflater;
    int[] mColorArray;
    int mColumns =0;
    int mRows = 0;
    DisplayHelper displayHelper;
    public ButtonFunActivityFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(savedInstanceState != null && savedInstanceState.getIntArray(COLOR_ARRAY)!= null){
            mColorArray = savedInstanceState.getIntArray(COLOR_ARRAY);
        }
        displayHelper = new DisplayHelper(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_button_fun, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        actionFunGrid = (GridLayout) view.findViewById(R.id.gl_button_fun);
         setActionFunGridPadding();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
    
    @Override
    public void onResume() {
        super.onResume();
        initScreen();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ib_color_button:
                int color = getRandomColor();
                v.setBackgroundColor(color);
                registerNewColor(v,color);
        }
    }
    
    public  void registerNewColor(View v , int color) {
        int position = ((Integer) v.getTag()).intValue();
        mColorArray[position] = color;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putIntArray(COLOR_ARRAY, mColorArray);
    }
    private void initScreen(){
        mColumns = displayHelper.getColumns();
        mRows = displayHelper.getRows();
        actionFunGrid.setColumnCount(mColumns);
        actionFunGrid.setRowCount(mRows);
        inflater = LayoutInflater.from(getContext());
        if(mColorArray==null){
            mColorArray = new int[mColumns*mRows];
            for(int i = 0; i<mColorArray.length;i++){
                mColorArray[i] =Integer.MIN_VALUE;
            }
        }
        for(int i = 0; i<  mColorArray.length; i++){
            View view = inflater.inflate(R.layout.color_button_item_image,null);
            ImageButton button = (ImageButton) view.findViewById(R.id.ib_color_button);
            int color;
            if(mColorArray[i]==Integer.MIN_VALUE){
                color = getRandomColor();
                mColorArray[i]= color;
            }else {
                color= mColorArray[i] ;
            }
            button.setBackgroundColor(color);
            button.setTag(Integer.valueOf(i));
            button.setOnClickListener(this);
            actionFunGrid.addView(view);
        }
    }
    private void setActionFunGridPadding(){
        actionFunGrid.setPadding(displayHelper.getSidePadding(),
                displayHelper.getVerticalPadding()
        ,displayHelper.getSidePadding(),
                displayHelper.getVerticalPadding());
    }
}
